import Vue from 'vue'
import Router from 'vue-router'

import IndexPage from '@/views/index'
import DetailPage from '@/views/detail'
import OrderListPage from '@/views/orderList'
import DetailAnaPage from '@/views/detail/analysis'
import DetailCouPage from '@/views/detail/count'
import DetailForPage from '@/views/detail/forecast'
import DetailPubPage from '@/views/detail/publish'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'index',
			component: IndexPage
		},
		{
			path: '/orderList',
			name: 'orderList',
			component: OrderListPage
		},
		{
			path: '/detail',
			component: DetailPage,
			redirect: '/detail/analysis',
			children: [
				{
					path: 'analysis',
					name: 'analysis',
					component: DetailAnaPage
				},
				{
					path: 'count',
					name: 'count',
					component: DetailCouPage
				},
				{
					path: 'forecast',
					name: 'forecast',
					component: DetailForPage
				},
				{
					path: 'publish',
					name: 'publish',
					component: DetailPubPage
				}
			]
		}
	]
})
